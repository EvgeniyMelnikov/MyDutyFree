<?php

namespace DutyFree\DataService;


interface DataServiceInterface
{
    public static function getArrayData(string $source): array;

    public static function getStringData(string $source): string;

    public static function setArrayData(string $source, array $data): void;

    public static function setStringData(string $source, string $data): void;
}
