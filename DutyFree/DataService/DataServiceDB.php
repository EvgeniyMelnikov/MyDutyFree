<?php

namespace DutyFree\DataService;


class DataServiceDB extends DataService
{
    private static $instance;

    const MESSAGE_DATA_FORMAT_ERROR = "Wrong data format. data: %s" . PHP_EOL;

    /**
     * @return DataServiceDB
     */
    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __clone(){}

    private function __construct(){}

    /**
     * @param string $source
     * @return array
     */
    public static function getArrayData(string $source): array
    {
        $data = [];
        // TODO: Implement setStringData() method.
        return $data;
    }

    /**
     * @param string $source
     * @return string
     */
    public static function getStringData(string $source): string
    {
        $data = '';

        // TODO: Implement setStringData() method.

        return $data;
    }

    public static function setArrayData(string $source, array $data): void
    {
        // TODO: Implement setStringData() method.
    }

    public static function setStringData(string $source, string $data): void
    {
        // TODO: Implement setStringData() method.
    }
}
