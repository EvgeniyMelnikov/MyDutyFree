<?php


namespace DutyFree\DataService;


use DutyFree\Exception\DutyFreeException;

abstract class DataService implements DataServiceInterface
{
    const MESSAGE_ERROR_STORAGE = "Unknown storage : %s";

    const STORAGE_FILE = 'file';
    const STORAGE_DB = 'db';


    const STORAGE = [
        self::STORAGE_FILE => DataServiceFile::class,
        self::STORAGE_DB => DataServiceDB::class
    ];

    /**
     * @param string $source
     * @param string $storage
     * @return array
     * @throws DutyFreeException
     */
    public static function getArrayDataFromStorage(string $source, string $storage): array
    {
        /** @var DataService $class */
        $class = self::getStorage($storage);

        return $class::getArrayData($source);
    }

    /**
     * @param string $source
     * @param string $storage
     * @return string
     * @throws DutyFreeException
     */
    public static function getStringDataFromStorage(string $source, string $storage): string
    {
        /** @var DataService $class */
        $class = self::getStorage($storage);

        return $class::getStringData($source);

    }

    /**
     * @param string $source
     * @param string $storage
     * @param array $data
     * @throws DutyFreeException
     */
    public static function setArrayDataToStorage(string $source, string $storage, array $data): void
    {
        /** @var DataService $class */
        $class = self::getStorage($storage);

        $class::setArrayData($source, $data);
    }

    /**
     * @param string $source
     * @param string $storage
     * @param string $data
     * @throws DutyFreeException
     */
    public static function setStringDataToStorage(string $source, string $storage, string $data): void
    {
        /** @var DataService $class */
        $class = self::getStorage($storage);

        $class::setStringData($source, $data);
    }

    /**
     * @param $array
     * @return array
     */
    public static function flatArray($array): array
    {
        array_walk($array, function (&$item) {

            foreach ($item as $k => $v) {
                if (is_array($v)) {
                    $item[$k] = json_encode($v);
                }
            }
        });

        return $array;
    }


    /**
     * @param string $storage
     * @return string
     * @throws DutyFreeException
     */
    private static function getStorage(string $storage): string
    {
        if (!isset(self::STORAGE[$storage])) {
            throw new DutyFreeException(sprintf(self::MESSAGE_ERROR_STORAGE, $storage));
        }

        return self::STORAGE[$storage];
    }
}
