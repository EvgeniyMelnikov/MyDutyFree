<?php

namespace DutyFree\DataService;

use DutyFree\Exception\DutyFreeException;
use DutyFree\MatchingService\MatchingService;

class DataServiceFile extends DataService
{
    const LENGTH_OF_READ = 1000;

    const MESSAGE_OPEN_FILE_ERROR = "Can't open file: ";

    const MESSAGE_CSV_FORMAT_ERROR = "Wrong CSV data format. File: '%s',  line: %s, data: %s" . PHP_EOL;

    /**
     * @param string $source
     * @return array
     * @throws DutyFreeException
     */
    public static function getArrayData(string $source): array
    {
        $data = [];
        if (!$fp = fopen($source, "r")) {
            throw  new DutyFreeException(self::MESSAGE_OPEN_FILE_ERROR . $source);
        }

        /** Get fields of data */
        $fields = fgetcsv($fp, self::LENGTH_OF_READ, ",");

        $countOfFields = count($fields);
        $line = 1;
        while (($row = fgetcsv($fp, self::LENGTH_OF_READ, ",")) !== false) {

            if (is_null($row)) {
                continue;
            }

            if ($countOfFields !== count($row)) {

                $log = vsprintf(self::MESSAGE_CSV_FORMAT_ERROR, [
                        $source,
                        $line,
                        implode(', ', $row)
                    ]
                );

                //@TODO Write to log file.
                echo $log . PHP_EOL;
                continue;
            }

            ++$line;
            $data[$row[0]] = array_combine($fields, $row);
        }

        fclose($fp);

        return $data;
    }

    /**
     * @param string $source
     * @return string
     * @throws DutyFreeException
     */
    public static function getStringData(string $source): string
    {
        if (!$products = file_get_contents($source)) {
            throw  new  DutyFreeException(self::MESSAGE_OPEN_FILE_ERROR . $source);
        }

        return $products;

    }


    /**
     * @param string $source
     * @param array $data
     * @param bool $rewrite
     * @param bool $csv
     * @throws DutyFreeException
     */
    public static function setArrayData(string $source, array $data, bool $rewrite = true, bool $csv = true): void
    {

        $mode = $rewrite ? 'w' : 'a';

        if (!$fp = fopen($source, $mode)) {
            throw  new DutyFreeException(self::MESSAGE_OPEN_FILE_ERROR . $source);
        }

        if ($csv) {
            $fields = array_keys(reset($data));
            fputcsv($fp, $fields);
        }

        foreach ($data as $fields) {
            $fields[MatchingService::FIELD_TITLE] = trim($fields[MatchingService::FIELD_TITLE], '"');
            fputcsv($fp, $fields);
        }

        fclose($fp);
    }

    /**
     * @param string $source
     * @param string $data
     * @throws DutyFreeException
     */
    public static function setStringData(string $source, string $data): void
    {
        if (!$products = file_put_contents($source, $data)) {
            throw  new  DutyFreeException(self::MESSAGE_OPEN_FILE_ERROR . $source);
        }
    }
}
