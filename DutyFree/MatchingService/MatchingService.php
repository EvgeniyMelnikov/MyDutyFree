<?php

namespace DutyFree\MatchingService;


class MatchingService
{
    const PATTERN = '/^\d+,("%s\s[[:print:]]*?")?("[[:print:]]*?\s%s\s[[:print:]]*?")?("[[:print:]]*?\s%s")?("%s")?,NULL$/ismu';

    const FIELD_TITLE = 'title';
    const FIELD_BRAND_ID = 'brand_id';
    const FIELD_PRODUCT_ID = 'product_id';
    const FIELD_BRAND_NOT_FOUND = 'brand_not_found';

    private static $brandsArray = [];
    private static $productsString = '';


    /**
     * @param array $rawData
     * @return array
     */
    public static function separateRawData(array $rawData): array
    {
        $matchData = [];

        $suggestData = [];

        foreach ($rawData as $id => $row) {
            if (count($row[self::FIELD_BRAND_ID]) == 1) {
                $matchData[$id] =
                    [
                        self::FIELD_PRODUCT_ID => $row[self::FIELD_PRODUCT_ID],
                        self::FIELD_TITLE => $row[self::FIELD_TITLE],
                        self::FIELD_BRAND_ID => $row[self::FIELD_BRAND_ID][0][self::FIELD_BRAND_ID],

                    ];
            }

            if (count($row[self::FIELD_BRAND_ID]) > 1) {
                $suggestData[$id] = $row;
            }
        }

        return [$matchData, $suggestData];
    }


    /**
     * @param array $brands
     * @param string $products
     * @return array
     */
    public static function getRawSearchData(array $brands, string $products): array
    {
        self::$brandsArray = $brands;
        self::$productsString = $products;
        $rawData = [];
        $brandsNotFound = [];
        foreach (self::searchBrandInProducts() as $rows) {
            if (isset($rows[self::FIELD_BRAND_NOT_FOUND])) {
                $brandsNotFound[] = $rows[self::FIELD_BRAND_NOT_FOUND];
                continue;
            }
            foreach ($rows as $row) {
                if (isset($rawData[$row[self::FIELD_PRODUCT_ID]])) {
                    $rawData[$row[self::FIELD_PRODUCT_ID]][self::FIELD_BRAND_ID][] = $row[self::FIELD_BRAND_ID];
                } else {
                    $rawData[$row[self::FIELD_PRODUCT_ID]] =
                        [
                            self::FIELD_PRODUCT_ID => $row[self::FIELD_PRODUCT_ID],
                            self::FIELD_TITLE => $row[self::FIELD_TITLE],
                            self::FIELD_BRAND_ID => [$row[self::FIELD_BRAND_ID]],

                        ];

                }
            }
        }

        return [$rawData, $brandsNotFound];
    }

    /**
     * @param array|null $brands
     * @param string|null $products
     * @return iterable
     */
    private static function searchBrandInProducts(array $brands = null, string $products = null): iterable
    {

        $brands = $brands ?? self::$brandsArray;
        $products = $products ?? self::$productsString;

        foreach ($brands as $brand) {
            $pattern = vsprintf(self::PATTERN,
                [
                    $brand[self::FIELD_TITLE],
                    $brand[self::FIELD_TITLE],
                    $brand[self::FIELD_TITLE],
                    $brand[self::FIELD_TITLE],
                ]
            );

            if (preg_match_all($pattern, $products, $match)) {
                $rawData = [];
                foreach ($match[0] as $line) {
                    list($id, $title,) = explode(',', $line);

                    $rawData[$id] = [
                        self::FIELD_PRODUCT_ID => $id,
                        self::FIELD_TITLE => $title,
                        self::FIELD_BRAND_ID => $brand,
                    ];
                }

                yield $rawData;
            } else {
                yield [self::FIELD_BRAND_NOT_FOUND => $brand];
            }
        }
    }
}
