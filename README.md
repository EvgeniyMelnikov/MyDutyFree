# MyDutyFree

Simple test task for matching data.  
Usage CSV files as source of data. 

### Dependency
* php 7.2

### Usage
Start script:
````sh
$ php idex.php
````
### Result

After work, the script creates 5 files into csv/ dir:

* products_brands.csv
* products_not_found_brand.csv
* products_match_brands.csv
* brands_not_found_in_products.csv
* products_to_manual_brand.csv