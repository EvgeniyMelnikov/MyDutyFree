<?php
 
use DutyFree\DataService\DataService;
use DutyFree\DataService\DataServiceFile;
use DutyFree\MatchingService\MatchingService;

define('APP', __DIR__ . '/');
define('CSV_DIR', APP . 'csv/');

spl_autoload_register(function ($class) {
    $class = APP . str_replace('\\', '/', $class) . '.php';
    include_once $class;
});


$fileBrand = 'brands.csv';
$fileProducts = 'products.csv';


try {

    $dataBrandArray = DataServiceFile::getArrayData(CSV_DIR . $fileBrand);
    $dataProductsArray = DataServiceFile::getArrayData(CSV_DIR . $fileProducts);
    $dataProductsString = DataServiceFile::getStringData(CSV_DIR . $fileProducts);
    [$rawData, $brandsNotFound] = MatchingService::getRawSearchData($dataBrandArray, $dataProductsString);

    /** @NOTE $suggestData can use for manual and automatic set probably Brand for Product */
    [$matchData, $suggestData] = MatchingService::separateRawData($rawData);

    $productsNotFoundBrand = array_diff_key($dataProductsArray, $rawData);
    $suggestDataFlat = DataService::flatArray($suggestData);
    $productBrand = array_replace($dataProductsArray, $matchData);

    echo "Amount: " . PHP_EOL;
    echo "All products in array: " . count($dataProductsArray) . PHP_EOL;
    echo "Products for find brand: " . count($rawData) . PHP_EOL;
    echo "Products which were not found in Brands: " . count($productsNotFoundBrand) . PHP_EOL;

    echo "Products with found brand : " . count($matchData) . PHP_EOL;
    echo "Products with found different brand: " . count($suggestData) . PHP_EOL;
    echo "Brands which were not found in products: " . count($brandsNotFound) . PHP_EOL;

    DataServiceFile::setArrayData(CSV_DIR . 'products_brands.csv', $productBrand);
    DataServiceFile::setArrayData(CSV_DIR . 'products_not_found_brand.csv', $productsNotFoundBrand);
    DataServiceFile::setArrayData(CSV_DIR . 'products_match_brands.csv', $matchData);
    DataServiceFile::setArrayData(CSV_DIR . 'brands_not_found_in_products.csv', $brandsNotFound);
    DataServiceFile::setArrayData(CSV_DIR . 'products_to_manual_brand.csv', $suggestDataFlat);


} catch (DutyFree\Exception\DutyFreeException $e) {
    echo $e->getMessage() . PHP_EOL;
} catch (\Exception $e) {
    echo $e->getTraceAsString() . PHP_EOL;
}

echo PHP_EOL . 'Look "csv/" folder' . PHP_EOL;
